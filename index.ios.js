// ISSUES :
// On enter room , sometimes only 10 results are shown
// On enter room , directly go to the last result

'use strict';

var React = require('react-native');
var {
  AppRegistry,
  Navigator,
  View,
} = React;

var GrpChat = require('./views/grpchat')
var ChatRoom = require('./views/chatroom')
var Authenticate = require('./views/authenticate')
var UserNamePicker = require('./views/userNamePicker')
var ImageSelector = require('./views/imageSelector')

var RootPage = React.createClass({

  renderScene(route, nav) {
    switch (route.id) {
      case 'main':
        return <GrpChat navigator={nav} />;
      case 'chatroom':
        return <ChatRoom navigator={nav} />;
      case 'authenticate':
        return <Authenticate navigator={nav} />;
      case 'namepicker':
        return <UserNamePicker navigator={nav} />;
      case 'imagepicker':
        return <ImageSelector navigator={nav} />;
      default:
        return <View />;
    }
  },

  render() {
    return (
      <Navigator
        initialRoute={{ id:"authenticate" }}
        renderScene={this.renderScene}
        configureScene={(route) => {
          if (route.sceneConfig) {
            return route.sceneConfig;
          }
          return Navigator.SceneConfigs.FloatFromRight;
        }} />
    );
  }
})

AppRegistry.registerComponent('RootPage', () => RootPage);
