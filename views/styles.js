'use strict';

var React = require('react-native');
var StyleSheet= React.StyleSheet;
var DeviceHeight = require('Dimensions').get('window').height;
var DeviceWidth = require('Dimensions').get('window').width;

module.exports = StyleSheet.create({


box: {
	shadowColor: "#242834",
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 1,
      width: 0
    },
    margin: 5,
    flex: 1, 
    backgroundColor: 'white'
},

shadow: {
	shadowColor: "#242834",
    shadowOpacity: 0.1,
    shadowRadius: 1.5,
    shadowOffset: {
      height: 0,
      width: 1
    },
},

centerALL:{
    //textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
},

bottomButton:{
	position:"absolute",bottom:0,left:0,
    right:0,height:80,backgroundColor:"#5BBE84"
},

buttonText:{
	fontSize: 30,
    fontFamily: "Avenir Book",
    color: '#ffffff',
},

defaultTab :{
	width:DeviceWidth,
	height:DeviceHeight,
	backgroundColor:"white"
},

pageTitle: {
	fontFamily: "Avenir",
	fontSize: 20,
	fontWeight: "bold",
    color:"white"
},

chatText: {
    fontFamily: "Avenir",
    fontSize: 14,
    color:"#16303C",
    fontWeight:"bold"
},

chatTextBy:{
    fontFamily: "Avenir",
    fontSize: 13,
    color:"#bdc3c7",
},

chatTextWhite: {
    fontFamily: "Avenir",
    fontSize: 14,
    color:"white",
    fontWeight:"bold"
},

chatTextByWhite:{
    fontFamily: "Avenir",
    fontSize: 13,
    color:"white",
},

pageSubTitle: {
    fontFamily: "Avenir",
    fontSize: 13,
    color:"white",
    fontWeight: "bold"
},

});