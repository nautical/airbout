'use strict';

var React = require('react-native');
var {
  AppRegistry,
  Navigator,
  View,
  StatusBarIOS,
  Text,
  DeviceEventEmitter,
  LayoutAnimation,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Image,
} = React;

var DeviceHeight = require('Dimensions').get('window').height;
var DeviceWidth = require('Dimensions').get('window').width;
var Styles = require('./styles');
var Icon = require('react-native-vector-icons/Ionicons');
var TimerMixin = require('react-timer-mixin');
require('regenerator/runtime');

var SERVER = "54.149.238.123"
//var SERVER = "localhost"

var _URL = "http://"+SERVER+":4000/api/v1/last50?room="

var authenticate = React.createClass({
  mixins: [TimerMixin],

  render: function() {
      StatusBarIOS.setStyle('light-content')

      return (
      <View style={{height:DeviceHeight,width:DeviceWidth,backgroundColor:"#4D5B7E"}}>
        <Image
              resizeMode={Image.resizeMode.cover}
              style={{width:DeviceWidth,height:DeviceHeight,opacity:0.1,position:"absolute",top:0}}
              source={require('image!dino')} />
        <Text style={{top:100,left:50,fontFamily: "Avenir Book",fontSize:38,color:"white",width:150,backgroundColor:"transparent"}}>Airbout</Text>
      </View>
    );
}});

module.exports = authenticate;
