'use strict';

var React = require('react-native');
var {
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} = React;

var deviceWidth = require('Dimensions').get('window').width;
var precomputeStyle = require('precomputeStyle');
var TAB_UNDERLINE_REF = 'TAB_UNDERLINE';
var Icon = require('react-native-vector-icons/Ionicons');

var styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: deviceWidth/3,
    height: 50
  },

  tabs: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
    borderWidth: 0,
  },
});

var DefaultTabBar = React.createClass({
  propTypes: {
    goToPage: React.PropTypes.func,
    activeTab: React.PropTypes.number,
    tabs: React.PropTypes.array
  },

  renderTabOption(name, page) {
    var isTabActive = this.props.activeTab === page;
    var lst = name.split(":")
    var _placeholder_ = <View></View>
    if(this.props.counter[page]>0){
      _placeholder_ = <View style={{width:20,height:20,backgroundColor:"white",borderRadius:10,
          position:"absolute",top:3,right:deviceWidth/6-25,alignItems: 'center',justifyContent: 'center'}}>
            <Text style={{fontSize:10}}>{this.props.counter[page]}</Text>
          </View>
    }
    return (
      <TouchableOpacity key={lst[0]} onPress={() => this.props.goToPage(page)}>
        <View style={[styles.tab]}>
          <Icon name={lst[1]} size={isTabActive ? 25 : 25} color="white" />
          <Text style={{color:"white",fontSize:12}}>{lst[0]}</Text>
            {_placeholder_}
        </View>
      </TouchableOpacity>
    );
  },

  setAnimationValue(value) {
    this.refs[TAB_UNDERLINE_REF].setNativeProps(precomputeStyle({
      left: (deviceWidth * value) / this.props.tabs.length
    }));
  },

  getStyle(){
    var numberOfTabs = this.props.tabs.length;

    if(this.props.inverse){
      return {
      position: 'absolute',
      width: deviceWidth / numberOfTabs,
      height: 4,
      backgroundColor: '#EA456B',
      top: 0,
      };
    }
    else{
      return {
        position: 'absolute',
        width: deviceWidth / numberOfTabs,
        height: 4,
        backgroundColor: '#475577',
        bottom: 0,
      };
    }
  },

  render() {
    var tabUnderlineStyle = this.getStyle();
    return (
      <View style={[styles.tabs,{backgroundColor:this.props.inverse?"#4B5980":"white"}]}>
        {this.props.tabs.map((tab, i) => this.renderTabOption(tab, i))}
        <View style={tabUnderlineStyle} ref={TAB_UNDERLINE_REF} />
      </View>
    );
  },
});

module.exports = DefaultTabBar;