'use strict';

var React = require('react-native');
var {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  SegmentedControlIOS,
  Navigator,
  ListView,
  TouchableOpacity,
  StatusBarIOS,
  AsyncStorage,
  ActivityIndicatorIOS,
  ScrollView,
  Modal,
  TextInput,
} = React;

require('regenerator/runtime');

var STORAGE_KEY = '@airbout:joined';

var Styles = require('./styles');
var DeviceHeight = require('Dimensions').get('window').height;
var DeviceWidth = require('Dimensions').get('window').width;
var ScrollableTabView = require('react-native-scrollable-tab-view');
var Tabbar = require('./DefaultInverseTab');
var Icon = require('react-native-vector-icons/Ionicons');
var TimerMixin = require('react-timer-mixin');

var RCTRefreshControl = require('react-refresh-control');

var dev = false;

var SERVER = "54.149.238.123"
//var SERVER = "localhost"

var BASE_URL = "http://"+SERVER+":4000"
var TRENDING_URL = "/api/v1/top10"

var flats = ["1ABC9C","2ECC71","3498DB","9B59B6","34495E","95A5A6","E74C3C","E67E22","F1C40F",
              "16A085","27AE60","2980B9","8E44AD","F39C12","D35400","C0392B","7F8C8D"]

var grpchat = React.createClass({
  mixins: [TimerMixin],

  async fetchData(){
    fetch(BASE_URL+TRENDING_URL)
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          trendingSource: responseData,
        });
      })
      .catch( (error) => {
        this.setState({
          error: error,
        });
      })
      .done( ()=>{
        this.setState({loadingTrending:false})
      } );
  },

  async _onValueChange(selectedValue) {
    if(this.state.selectedValues.indexOf(selectedValue)==-1){
        this.setState({
          selectedValues: this.state.selectedValues.concat([selectedValue]),
        });
    }
    else if(this.state.selectedValues.indexOf(selectedValue)>-1){
      var index = this.state.selectedValues.indexOf(selectedValue)
      this.state.selectedValues.splice(index,1)
      var temp = this.state.selectedValues;
      this.setState({
          selectedValues: temp
        });
    }
    else{
      return
    }

  },

  async _removeStorage() {
    try {
      await AsyncStorage.removeItem(STORAGE_KEY);
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  },

  async _loadInitialState() {
    try {
      var values = await AsyncStorage.getItem(STORAGE_KEY);
      if (values !== null){
        var parsed = JSON.parse(values);
        var keys = Object.keys(parsed).map(Number);
        this.setState({
          selectedValues: keys,
          idRoomMap: parsed
        });
      }
    } catch (error) {
      console.log('AsyncStorage error: ' + error.message);
    }
  },

  componentDidMount () {
    
    this.fetchData().done( ()=>{
      
      this.setTimeout(
            () => { 
                
                RCTRefreshControl.configure({
                  node: this.refs['_scrollview_'],
                  tintColor: '#4D5B7E',
                  activityIndicatorViewColor: '#4D5B7E'
                }, () => {
                  this.fetchData().done(() => {
                    RCTRefreshControl.endRefreshing(this.refs['_scrollview_']);
                  });
                });


            },
            500
          );
    } );
      if(dev==true){
        this._removeStorage().done(); // remove storage if dev
      }
      this._loadInitialState().done();


  },

  sendMessage : function(str) {
    this.state.chan.push("new:msg", {user: "MOBILE", body: str })
  },

  handleChangeTab: function(i,obj){
      this.setState({counterValues: [0,0,0]})
      var title = ""
      if(i.i==0){
        title = "Trending"
        this.setState({search:1,folder:0});
      }
      if(i.i==1){
        title = "My Rooms"
        this.setState({search:0,folder:0});
      }
      if(i.i==2){
        title = "Settings"
        this.setState({search:0,folder:0});
      }
      this.setState({
        page: title
      })
  },

  changeRoom: function(rowdata){
      this.props.navigator.push({
        id: 'chatroom',
        sceneConfig: Navigator.SceneConfigs.PushFromRight,
        passProps: {
                roomInfo : {id:rowdata,room:this.state.idRoomMap[rowdata]},
              }
      });

  },

  renderMyRoom : function(rowdata){
    return(
      <TouchableOpacity onPress={this.changeRoom.bind(this,rowdata)}>
        <View style={{
          width:DeviceWidth,height:100,alignItems: 'center',
          borderBottomWidth:.5,borderColor:"#F1F1F1",flexDirection:"row"
        }}>
        <View style={[Styles.centerALL,
          {width:60,height:60,borderRadius:30,
            marginLeft:20,marginRight:10,
            backgroundColor:"#"+flats[rowdata]
          }]}>
          <Text style={{fontSize:25,color:"white"}}>{this.state.idRoomMap[rowdata][0].toUpperCase()}</Text>
        </View>
        <View style={{flexDirection:"column" }}>
          <Text style={{fontSize:18,fontFamily:"Avenir Book",fontWeight:"bold",color:"#9B9B9B"}}>
            {this.state.idRoomMap[rowdata]}
          </Text>
          <Text style={{fontSize:13,fontFamily:"Avenir Book",color:"#9B9B9B"}}>
            Joined 23-May-2015
          </Text>
        </View>
       <View style={[Styles.centerALL,{position:"absolute",height:99,right:20}]}>
            <Icon name="arrow-right-b" size={25} color="#CED4E9" />
        </View>
        </View>
      </TouchableOpacity>
    )
  },

  pressed:  function(obj){
      this._onValueChange(obj.id).done( ()=>{this.setState({counterValues: [0,this.state.selectedValues.length,0]})} );
      var temp = this.state.idRoomMap
      if(this.state.idRoomMap.hasOwnProperty(obj.id)){
        delete temp[obj.id];
      }
      else{
        temp[obj.id] = obj.room
      }
      this.setState({
        idRoomMap: temp
      })
      AsyncStorage.setItem(STORAGE_KEY, JSON.stringify(temp) );

  },

  renderTrendingRow : function(rowdata){

    if(this.state.selectedValues.indexOf(rowdata.id)>-1){
      return
    }

    return(
      <View style={{
        width:DeviceWidth,height:90,alignItems: 'center',
        borderBottomWidth:.5,borderColor:"#F1F1F1",flexDirection:"row"
      }}>
      
      <View style={[Styles.centerALL,
        {width:60,height:60,borderRadius:30,
          marginLeft:20,marginRight:10,
          backgroundColor:"#"+flats[rowdata.id]
        }]}>
        <Text style={{fontSize:25,color:"white"}}>{rowdata.room[0].toUpperCase()}</Text>
      </View>

      <View style={{flexDirection:"column",width:DeviceWidth-160 }}>
        <Text style={{fontSize:16,fontFamily:"Avenir Book",fontWeight:"bold",color:"#9B9B9B"}}>#{rowdata.room}</Text>
        <Text style={{fontSize:16,fontFamily:"Avenir Book",color:"#C2C2C6"}}>{rowdata.count} Members</Text>
      </View>

     <View style={[Styles.centerALL,{position:"absolute",right:20,
     flexDirection:"row",height:89,width:50}]}>
       <TouchableOpacity onPress={this.pressed.bind(this,rowdata)}>
          <View style={[Styles.centerALL,{borderWidth:1,
            borderColor: this.state.selectedValues.indexOf(rowdata.id)>-1?"black":"#EA456B",borderRadius:5,width:50}]}>
            <Text style={{fontSize:13,fontWeight:"bold",fontFamily:"Avenir Book",
            color:this.state.selectedValues.indexOf(rowdata.id)>-1?"black":"#EA456B",margin:8}}>
            {this.state.selectedValues.indexOf(rowdata.id)>-1?"exit":"join"}
            </Text>
          </View>
       </TouchableOpacity>
      </View>

      </View>
    )
  },

  closeModal: function(){
    this.refs._roomname_.setNativeProps({text:''});
    this.setState({
      modalOpened: false,
      createNewRoomStatus:'',
      _roomname_:'',
    })
  },

  closesearchModal: function(){
    this.refs._searchroomname_.setNativeProps({text:''});
    this.setState({
      searchmodalOpened: false,
      _searchroomname_:'',
      searchResults:[]
    })
  },

  searchRoom: function(){
    var roomName = this.state._searchroomname_;
    if(roomName=="" || roomName == null || roomName == undefined){
      return
    }
    if (roomName.match(/[\W_]/)){
      return
    }
    this.setState({isSearching:true})
    fetch(BASE_URL+"/api/v1/search?q="+roomName)
      .then((response) => response.json())
      .then((responseData) => {
          this.setState({searchResults: responseData})
      })
      .catch( (error) => {
        this.setState({
          error: error,
        });
      })
      .done( ()=>{
        this.setState({isSearching:false})
      } );

      this.refs._searchroomname_.setNativeProps({text:''});
      this.setState({
        _searchroomname_:'',
      })

  },

  createNewRoom: function(){
    var roomName = this.state._roomname_;
    if(roomName=="" || roomName == null || roomName == undefined){
      return
    }
    if (roomName.match(/[\W_]/)){
      this.setState({createNewRoomStatus:'Sorry No spaces or special characters allowed'})
      return
    }
    fetch(BASE_URL+"/api/v1/createRoom?room="+roomName)
      .then((response) => response.json())
      .then((responseData) => {
        if(responseData.status=="success"){
          this.setState({createNewRoomStatus:'Success ! Room Created , It should now appear in search'})
        }
        else{
          this.setState({createNewRoomStatus:'Oops ! looks like room exist , Join instead ?'})
        }
      })
      .catch( (error) => {
        this.setState({
          error: error,
        });
      })
      .done();
    
    this.refs._roomname_.setNativeProps({text:''});
    this.setState({
      _roomname_:'',
    })

  },

  getInitialState: function() {
    
    return {
        page: "Trending",
        search: 1,
        folder: 0,

        selectedValues: [],

        modalOpened: false,
        searchmodalOpened: false,
        searchResults: [],

        counterValues: [0,0,0],

        loadingTrending: true,

        isSearching: false,

        createNewRoomStatus: "",

        chan: null,
        messages: "",

        trendingSource: [],

        idRoomMap: {},

    };
  },

  render: function() {
    StatusBarIOS.setStyle('light-content')

    var par = this;
    var _emptyView_ = <View style={[Styles.centerALL,{width:DeviceWidth,height:DeviceHeight-115,
                    position:"absolute",top:0,left:0,opacity:this.state.loadingTrending?1:0}]}>
            <ActivityIndicatorIOS animating={this.state.loadingTrending} style={{height: 80}} size="small"/>
          </View>

    var _emptyViewMyChat_ = <View></View>

    if(
      this.state.trendingSource.map(function(a) {return a.id}).sort().toString() == 
      this.state.selectedValues.sort().toString() &&
      this.state.trendingSource.length != 0
      ){
      _emptyView_ = <View style={[Styles.centerALL,{width:DeviceWidth,height:200}]}>
                      <Text style={{fontSize:13,color:"#4D5B7E"}}>Oh !! you are awesome and all caught up!</Text>
                      <Text style={{fontSize:13,marginTop:10,color:"#4D5B7E"}}>We will update trending rooms </Text>
                      <Text style={{fontSize:13,color:"#4D5B7E"}}>when we find one for you</Text>
                    </View>
    }

    if(this.state.selectedValues.length == 0){
      _emptyViewMyChat_ = <View style={[Styles.centerALL,{width:DeviceWidth,height:200}]}>
                      <Text style={{fontSize:13,color:"#4D5B7E"}}>Oh oo!! you need to join some rooms!</Text>
                      <Text style={{fontSize:13,marginTop:10,color:"#4D5B7E"}}>Check TRENDING rooms</Text>
                      <Text style={{fontSize:13,color:"#4D5B7E"}}>or try SEARCH eg: iitjee, ibps, generalawareness</Text>
                    </View>
    }

    return (
      <View style={{height:DeviceHeight,width:DeviceWidth,flexDirection:"column"}}>
        <View style={{backgroundColor:"#4B5980",height:20}}></View>
        <View style={[Styles.centerALL,
          {height:60,width:DeviceWidth,backgroundColor:"#4B5980"}]}>
          <TouchableOpacity style={[Styles.centerALL,{position:"absolute",left:10,top:10,
            opacity:this.state.search,width:45,height:45}]}
             onPress={ ()=>{ this.setState({searchmodalOpened:true}) } }>
          <Icon name="search" size={25} style={{opacity:this.state.search}} color="white" />
          </TouchableOpacity>
          <Text style={Styles.pageTitle}>{this.state.page}</Text>
          <Icon name="document-text" style={{position:"absolute",right:20,top:20,opacity:this.state.folder}} size={25} color="white" />
        </View>

          <ScrollableTabView counter={this.state.counterValues} inverse={true} onChangeTab={this.handleChangeTab} 
            renderTabBar={() => <Tabbar activeTab={1} inverse={true} />} >

            <View tabLabel="Trending:pound" style={Styles.defaultTab} >
                <View style={{height:DeviceHeight-130}}>
                  
                  <TouchableOpacity onPress={()=>{this.setState({modalOpened:true})}}>
                    <View style={[Styles.centerALL,{width:DeviceWidth-40,left:20,marginTop:20,marginBottom:20,
                      borderRadius:5,backgroundColor:"#EA456B",height:40}]}>
                      <Text style={{color:"white",fontSize:15,fontFamily:"Avenir Book",fontWeight:"bold"}}>
                        Add a new public room
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <ScrollView
                    ref="_scrollview_"
                    automaticallyAdjustContentInsets={false}
                    style={{height:DeviceHeight-130}}>
                        {Array.prototype.map.call(this.state.trendingSource,
                          function(source,i){
                            return par.renderTrendingRow(source)
                          }
                        )}
                        {_emptyView_}
                  </ScrollView>
                </View>
            </View>

            <View tabLabel="My rooms:chatbubble-working" style={Styles.defaultTab} >
              <View style={[Styles.centerALL,{width:DeviceWidth,height:DeviceHeight-130,marginTop:10}]}>
                <ScrollView
                    automaticallyAdjustContentInsets={false}
                    style={{height:DeviceHeight-130}}>
                  {Array.prototype.map.call(this.state.selectedValues,
                    function(source,i){
                      return par.renderMyRoom(source)
                    }
                  )}
                  {_emptyViewMyChat_}
                </ScrollView>
              </View>
            </View>

            <View tabLabel="Settings:gear-b" style={Styles.defaultTab} />
          
          </ScrollableTabView>

          <Modal
          animated={true}
          transparent={true}
          visible={this.state.modalOpened}>
            <View style={{backgroundColor:"white",opacity:1,width:DeviceWidth,height:DeviceHeight}}>
              
              <View style={[
                Styles.centerALL,
                {position:"absolute",top:1,width:DeviceWidth,height:50,backgroundColor:"transparent"}
              ]}>
                <Text style={{color:"grey",fontSize:13,fontWeight:"bold"}}>Create room</Text>
              </View>

              <TouchableOpacity onPress={this.closeModal}
                  style={[Styles.centerALL,{position:"absolute",top:0,right:0,width:68,height:58}]}>
                <Icon name="close" size={25} color="#D85B6F" />
              </TouchableOpacity>

              <View style={[Styles.centerALL,{position:"absolute",top:50,width:DeviceWidth}]}>
                  <TextInput
                  style={{width:DeviceWidth-120,backgroundColor:"#EBEBEB",height:35,
                        marginLeft:60,padding:5,borderRadius:5,fontSize:13,marginBottom:20}}
                  multiline={false}
                  placeholder="An Awesome unique name"
                  autoFocus={false}
                  ref="_roomname_"
                  onChangeText={(_roomname_) => this.setState({_roomname_})}
                  value={this.state._roomname_} />

                  <TouchableOpacity style={Styles.centerALL} onPress={this.createNewRoom}>
                  <View style={[Styles.centerALL,{width:DeviceWidth-120,backgroundColor:"#4D5B7E",
                      height:35,padding:5,borderRadius:5}]}>
                    <Text style={{color:"white",fontSize:13}}>Done !</Text>
                  </View>
                  <Text ref="createNewRoomStatus" style={{fontSize:13,color:"#4D5B7E",margin:10}}>{this.state.createNewRoomStatus}</Text>
                </TouchableOpacity>

              </View>
            </View>
          </Modal>


          <Modal
          animated={true}
          transparent={true}
          visible={this.state.searchmodalOpened}>
            <View style={{backgroundColor:"white",opacity:1,width:DeviceWidth,height:DeviceHeight}}>
              
              <View style={[
                Styles.centerALL,
                {position:"absolute",top:1,width:DeviceWidth,height:50,backgroundColor:"transparent"}
              ]}>
                <ActivityIndicatorIOS animating={this.state.isSearching} size="small" 
                  style={{opacity:this.state.isSearching?1:0}} />
                <Text 
                  style={{color:"grey",fontSize:13,fontWeight:"bold",opacity:this.state.isSearching?0:1,marginTop:-15}}>
                  Search
                </Text>
              </View>

              <TouchableOpacity onPress={this.closesearchModal}
                  style={[Styles.centerALL,{position:"absolute",top:0,right:0,width:68,height:58}]}>
                <Icon name="close" size={25} color="#D85B6F" />
              </TouchableOpacity>

              <View style={[Styles.centerALL,{position:"absolute",top:50,width:DeviceWidth}]}>
                  <TextInput
                  style={{width:DeviceWidth-120,backgroundColor:"#EBEBEB",height:35,
                        marginLeft:60,padding:5,borderRadius:5,fontSize:13,marginBottom:20}}
                  multiline={false}
                  placeholder="Search a room"
                  autoFocus={false}
                  ref="_searchroomname_"
                  onChangeText={(_searchroomname_) => this.setState({_searchroomname_})}
                  onSubmitEditing={ ()=>{ this.searchRoom() } }
                  value={this.state._searchroomname_} />

                  <TouchableOpacity style={Styles.centerALL} onPress={this.searchRoom}>
                  <View style={[Styles.centerALL,{width:DeviceWidth-120,backgroundColor:"#4D5B7E",
                      height:35,padding:5,borderRadius:5}]}>
                    <Text style={{color:"white",fontSize:13}}>Search</Text>
                  </View>
                </TouchableOpacity>
                <View style={{flexDirection:"row",marginTop:20}}>
                {Array.prototype.map.call(this.state.searchResults,
                          function(source,i){
                            return (
                              <TouchableOpacity onPress={par.pressed.bind(this,{id:source.id,room:source.room})}>
                              <View style={[Styles.centerALL,
                                  {height:50,width:DeviceWidth/3-10,borderRadius:5,
                                    marginLeft:5,marginRight:5,
                                    backgroundColor:par.state.selectedValues.indexOf(source.id)>-1?
                                      "#B4D264":"#D85B6F"}]}>
                                <Text key={source.room} style={{color:"white",fontSize:13}}>
                                  {source.room.toUpperCase()}
                                </Text>
                                <Text style={{position:"absolute",bottom:5,right:5,fontSize:10,color:"white"}}>
                                 {par.state.selectedValues.indexOf(source.id)>-1?
                                  "Member":"Join"}
                                </Text>
                              </View>
                              </TouchableOpacity>
                            )
                          }
                        )}
                </View>
              </View>
            </View>
          </Modal>


      </View>

      
    );
  }
});

module.exports = grpchat;
