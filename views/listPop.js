"use strict";

var React = require('react-native');
var SCREEN_HEIGHT = require('Dimensions').get('window').height;
var {
  ListView,
  PropTypes,
  StyleSheet,
  Text,
  TouchableOpacity,
  ScrollView,
  View
} = React;
var noop = () => {};
var ds = new ListView.DataSource({rowHasChanged: (r1,r2)=>(r1!==r2)});
var Styles = require('./styles')

var ListPopover = React.createClass({
  propTypes: {
    list: PropTypes.array.isRequired,
    isVisible: PropTypes.bool,
    onClick: PropTypes.func,
    onClose: PropTypes.func,
  },
  getDefaultProps: function() {
    return {
      list: [""],
      isVisible: false,
      onClick: noop,
      onClose: noop,
    };
  },
  getInitialState: function() {
    return {
      wrongAns: "",
      showAns: false,
      responseText: "",
      responseTextColor:"transparent"
    };
  },

  highlight: function(){
    this.setState({
      showAns:true,
    });
  },
  handleClick: function(data) {
    if(data==this.props.list[this.props.correctAns]){
      this.setState({
        responseText:"Awesome ! " + this.props.desc,
        responseTextColor: this.props.optionsColor || "#1abc9c"
      })
    }
    else{
      this.setState({
        responseText:"Opps ! " + this.props.desc,
        responseTextColor: this.props.optionsColor || "#4D5B7E"
      })
    }
    this.highlight();
    this.setState({
      wrongAns:data
    })
    this.props.onClick(data);
    this.props.onClose();
  },
  renderRow: function(rowData) {
    var separatorStyle = this.props.separatorStyle || DefaultStyles.separator;
    var rowTextStyle = this.props.rowText || DefaultStyles.rowText;

    var separator = <View style={[separatorStyle,{backgroundColor:this.props.optionsColor||'#ccc'}]}/>;
    if (rowData === this.props.list[0]) {
      separator = {};
    }

    var row = <View style={[Styles.centerAll,{flexDirection:"row",height:30}]}>
    <View style={{
      width:18,height:18,
      borderRadius:9,margin:5,
      backgroundColor:
        (this.state.showAns && rowData==this.props.list[this.props.correctAns])
            ?
            "#2ecc71":this.state.showAns?"#e67e22":"#EBEBEB"
      }}></View>
    <Text style={[rowTextStyle,{color:this.props.optionsColor||"#ccc"}]}>{rowData}</Text>
    </View>

    return (
      <View>
        {separator}
        <TouchableOpacity onPress={ this.handleClick.bind(this,rowData) }>
          {row}
        </TouchableOpacity>
      </View>
    );
  },

  render: function() {
    var containerStyle = this.props.containerStyle || DefaultStyles.container;
    var popoverStyle = this.props.popoverStyle || DefaultStyles.popover;
    var par = this
    return (
      <View >
      <TouchableOpacity onPress={this.props.onClose}>
        <View style={containerStyle}>
          <View style={popoverStyle}>
            {this.props.list.map( function(a) {
                return par.renderRow(a)
            })}
          </View>
            <Text style={{
              fontSize:10,color:this.state.responseTextColor,
              padding:5,borderWidth:.5,borderColor:this.state.responseTextColor,
              borderRadius:5
            }}>{this.state.responseText}</Text>
        </View>
      </TouchableOpacity>
      </View>
    );
  }
});


var DefaultStyles = StyleSheet.create({
  container: {
    top: 0,
    bottom: 10,
    left: 0,
    right: 0,
    position: 'absolute',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  popover: {
    margin: 10,
    borderRadius: 3,
    padding: 3,
    backgroundColor: 'transparent',
  },
  rowText: {
    padding: 0,
    height: 30,
    padding:5,
  },
  separator: {
    margin:2,
    height: 0.5,
    marginLeft: 8,
    marginRight: 8,
  },
});

module.exports = ListPopover;