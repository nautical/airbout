'use strict';

var React = require('react-native');
var {
  AppRegistry,
  Navigator,
  View,
  StatusBarIOS,
  Text,
  DeviceEventEmitter,
  Animated,
  TouchableOpacity,
  TextInput,
  Easing,
  ScrollView,
  Image,
} = React;

var DeviceHeight = require('Dimensions').get('window').height;
var DeviceWidth = require('Dimensions').get('window').width;
var Styles = require('./styles');
var Icon = require('react-native-vector-icons/Ionicons');
var TimerMixin = require('react-timer-mixin');
require('regenerator/runtime');

var SERVER = "54.149.238.123"
//var SERVER = "localhost"

var _URL = "http://"+SERVER+":4000/api/v1/last50?room="

var authenticate = React.createClass({
  mixins: [TimerMixin],

  render: function() {
      return (
      <View style={{height:DeviceHeight,width:DeviceWidth,backgroundColor:"#4D5B7E"}}>
        <Image
              resizeMode={Image.resizeMode.cover}
              style={{width:DeviceWidth,height:DeviceHeight,opacity:0.1,position:"absolute",top:0}}
              source={require('image!dino')} />

        <View style={[{position:"absolute",top:70,width:DeviceWidth,height:200,backgroundColor:"transparent"}]}>
            
            <Text style={{fontSize:18,color:"white",textAlign: 'center'}}>
              Select a gender <Text style={{fontWeight:"bold",color:"#B4D264"}}>neutral name</Text>
            </Text>
            <Text style={{fontSize:11,color:"white",textAlign: 'center'}}>
              Please prevent using real name
            </Text>

            <TextInput
              style={{left:60,width:DeviceWidth-120,backgroundColor:"#CED4E9",
                height:35,marginTop:15,borderRadius:5,padding:5}}
              multiline={false}
              placeholder={"eg: sage, quinn, harper etc."}
              autoFocus={false}/>

            <TouchableOpacity>
                  <View style={[Styles.centerALL,{left:60,width:DeviceWidth-120,backgroundColor:"#EA456B",
                      height:35,padding:5,borderRadius:5,marginTop:15}]}>
                    <Text style={{fontWeight:"bold",color:"white",fontSize:13}}>Done !</Text>
                  </View>
            </TouchableOpacity>

        </View>
      </View>
    );
  }});

module.exports = authenticate;
