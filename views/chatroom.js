'use strict';

var React = require('react-native');
var {
  AppRegistry,
  Navigator,
  View,
  StatusBarIOS,
  Text,
  DeviceEventEmitter,
  Animated,
  TouchableOpacity,
  TextInput,
  Modal,
  Easing,
  ListView,
  ScrollView,
  Image,
} = React;

var DeviceHeight = require('Dimensions').get('window').height;
var DeviceWidth = require('Dimensions').get('window').width;
var Styles = require('./styles');
var Icon = require('react-native-vector-icons/Ionicons');
var RCTUIManager = require('NativeModules').UIManager;
var TimerMixin = require('react-timer-mixin');
require('regenerator/runtime');

var Socket = require('../phoenix/phoenix').Socket
var LongPoller = require('../phoenix/phoenix').LongPoller

var ListPop = require('./listPop')

var _keyboardWillShowSubscription = null
var _keyboardWillHideSubscription = null
var socket = null

var SERVER = "54.149.238.123"
//var SERVER = "localhost"

var _URL = "http://"+SERVER+":4000/api/v1/last50?room="

var myname = "Mobile"

var chatroom = React.createClass({
  mixins: [TimerMixin],

  componentWillMount (){
    var base_OBJ = {room:"ibps"}
    //var base_OBJ = this.props.navigator._navigationContext._currentRoute.passProps.roomInfo;
    this.setState({
      page: base_OBJ.room
    })
  },

  componentWillUnmount() {
    _keyboardWillShowSubscription.remove();
    _keyboardWillHideSubscription.remove();
  },

  componentDidMount() {
    _keyboardWillShowSubscription = DeviceEventEmitter.addListener('keyboardWillShow', (e) => this._keyboardWillShow(e));
    _keyboardWillHideSubscription = DeviceEventEmitter.addListener('keyboardWillHide', (e) => this._keyboardWillHide(e));
    this.fetchData().done(()=>{
      this.joinRoom(this.state.page).done();
    });
    this._data = [];
  },

  _keyboardWillShow: function(e){

    RCTUIManager.measure(this.refs.scrollview.getInnerViewNode(), (...data)=>{
          Animated.timing(
            this.state.keyboardOffset,
          {
            toValue: e.endCoordinates.height,
            easing: Easing.inOut(Easing.bezier(0.42, 0, 1, 1,250)),
            duration: 250,
          }).start();

    
          this.setState({
            keyboardHeight: e.endCoordinates.height
          })

          if(data[3]<DeviceHeight-e.endCoordinates.height-115){
            this.refs.scrollview.scrollTo(0)
          }
          else{
            this.refs.scrollview.scrollTo(data[3]-DeviceHeight+e.endCoordinates.height+120)
          }


    });
  
  },

  _keyboardWillHide: function(e){
    
    RCTUIManager.measure(this.refs.scrollview.getInnerViewNode(), (...data)=>{

          Animated.timing(
            this.state.keyboardOffset,
          {
            toValue: 0,
            easing: Easing.inOut(Easing.bezier(0.42, 0, 1, 1,250)),
            duration: 300,
            delay: 0,
          }).start();

          this.setState({
            keyboardHeight: 0
          })

          if(data[3]<DeviceHeight-115){
            this.refs.scrollview.scrollTo(0)
          }
          else{
            this.refs.scrollview.scrollTo(data[3]-DeviceHeight+115)
          }
    });

  },


  bringFocus: function(){
    RCTUIManager.measure(this.refs.scrollview.getInnerViewNode(), (...data)=>{
          if(data[3]<DeviceHeight-this.state.keyboardHeight){
            this.refs.scrollview.scrollTo(0)
          }
          else{
            this.refs.scrollview.scrollTo(data[3]+this.state.keyboardHeight-DeviceHeight+120)
          }
    });
  },

  async fetchData() {
    var base_OBJ = {room:"ibps"}
    //var base_OBJ = this.props.navigator._navigationContext._currentRoute.passProps.roomInfo;
    fetch(_URL+base_OBJ.room)
      .then((response) => response.json())
      .then((responseData) => {
        this._data = this._data.concat(responseData.reverse());
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(this._data)
        });

        this.setTimeout(
            () => { this.setState({loaded:true});},
            500
        );

      })
      .catch( (error) => {
        this.setState({
          error: error
        });
      })
      .done();
  },


  switchit(){
    socket.disconnect();
    this.props.navigator.pop();
  },

  refresh(){
    socket.disconnect();

    this._data = [];
    this.setState({
            dataSource: this.state.dataSource.cloneWithRows(this._data),
            count : "0"
          });
    this.setTimeout(
            () => { 
              this.fetchData().done(()=>{
                this.joinRoom(this.state.page).done();
              })
          },500);
  },

  async joinRoom(room){

    socket = new Socket("ws://"+SERVER+":4000/socket", {params: {userToken: "123"}} );

    socket.connect()

    socket.onOpen( ev => console.log("OPEN:", ev) )
    socket.onError( ev => console.log("ERRORR:", ev) )
    socket.onClose( e => console.log("CLOSE:", e))

    var chan = socket.channel("rooms:"+room, {})

    chan.join().receive("ignore", () => console.log("auth error"))
                .receive(
                   "ok", function(){
                       console.log("OK");
                   }
                 )
    chan.onError(e => console.log("something went wrong", e))
    chan.onClose(e => console.log("channel closed", e))

    chan.on("new:msg", msg => {
        if(msg.user=="SYSTEM"){
          this.setState({
            count: JSON.parse(msg.body).count,
          });
        }
        else if(msg.user=="INFO"){
          // TODO and TO THINK
        }
        else{
          var _t = new Date();
          this._data = this._data.concat({msg:msg.body,user:msg.user,created_at:_t.toString()});
          this.setState({
            dataSource: this.state.dataSource.cloneWithRows(this._data),
          });
          this.setTimeout(
            () => { this.bringFocus(); },
            500
          );
        }
    })
    this.setState({
      chan: chan
    })
  },

  sendMessage : function(str) {
    if(str==null || str==undefined){
      return
    }
    this.refs.textinput.setNativeProps({text: ''})
    this.state.chan.push("new:msg", {user: myname , body: str })
  },

  hideKeyboard: function(){
    this.refs.textinput.blur()
  },

  getInitialState: function() {
    return {
        chan: null,
        messages:"",
        page: "",
        count: "",
        loaded: false,
        modalOpened: false,
        keyboardOffset: new Animated.Value(0),
        dataSource: new ListView.DataSource({
          rowHasChanged: (row1, row2) => row1 !== row2,
        }),
        keyboardHeight: 0,
    };
  },

  getTime: function(timeStr){
    var _t = new Date(timeStr)
    var _time = _t.toLocaleTimeString().split(" ")
    var timeStr = _time[0].split(":")[0]+":"+_time[0].split(":")[1]+" "+_time[1]

    var _date = _t.toLocaleDateString().split(",")[0].split(" ")
    var dateStr = _date[0].substring(0,3)+" "+_date[1]
    return dateStr+", "+timeStr
  },

  renderListItem: function(rowData) {
    if(rowData.user==myname){
        try {
            var o = JSON.parse(rowData.msg);
              if (o && typeof o === "object" && o !== null) {
                  return (
                    <View style={{flex:1,flexDirection:"row",width:(3*DeviceWidth/4),marginTop:10,marginLeft:DeviceWidth/4}}>
                        <View style={[{width:(3*DeviceWidth/4)-25,paddingLeft:10,paddingRight:10,paddingTop:10,paddingBottom:20,marginLeft:-1,
                          backgroundColor:"#B4D264",borderRadius:5,flexDirection:"column"} ]}>
                          <Text style={Styles.chatTextByWhite}>Choose an option</Text>
                          <Text style={Styles.chatTextWhite}>{o.q}</Text>
                          <View style={{width:(3*DeviceWidth/4)-50,height:200}}>
                          <ListPop
                              list={[o.a,o.b,o.c,o.d]}
                              isVisible={true}
                              onClose={this.closePopover}
                              correctAns={o.ans}
                              desc={o.desc}
                              optionsColor={"white"}
                            />
                          </View>
                          <Text style={[Styles.chatTextByWhite,{position:"absolute",right:5,bottom:0}]}>
                            {this.getTime(rowData.created_at)}
                          </Text>
                        </View>
                        <Icon name="arrow-right-b" size={25} color="#B4D264" />
                    </View>
                  );
              }
          }
          catch(e){
          return (<View style={{flex:1,flexDirection:"row",width:(3*DeviceWidth/4),marginTop:10,marginLeft:DeviceWidth/4}}>
            <View style={[{width:(3*DeviceWidth/4)-15,paddingLeft:10,paddingRight:10,paddingTop:10,paddingBottom:20,
              backgroundColor:"#B4D264",borderRadius:5} ]}>
              <Text style={Styles.chatTextWhite}>{rowData.msg}</Text>
              <Text style={[Styles.chatTextByWhite,{position:"absolute",right:5,bottom:0,opacity:0.5}]}>
                {this.getTime(rowData.created_at)}
              </Text>
            </View>
            <Icon name="arrow-right-b" size={25} color="#B4D264" style={[{marginLeft:-1}]} />
          </View>)
        }
    }
    else{
          try {
            var o = JSON.parse(rowData.msg);
              if (o && typeof o === "object" && o !== null) {
                  return (
                    <View style={{flex:1,flexDirection:"row",width:(3*DeviceWidth/4),marginTop:10,marginLeft:5}}>
                        <Icon name="arrow-left-b" size={25} color="white" />
                        <View style={[{width:(3*DeviceWidth/4)-25,paddingLeft:10,paddingRight:10,paddingTop:10,paddingBottom:20,marginLeft:-1,
                          backgroundColor:"white",borderRadius:5,flexDirection:"column"} ]}>
                          <Text style={Styles.chatTextBy}>{rowData.user} has asked a question</Text>
                          <Text style={Styles.chatTextBy}>Choose an option</Text>
                          <Text style={Styles.chatText}>{o.q}</Text>
                          <View style={{width:(3*DeviceWidth/4)-50,height:200}}>
                          <ListPop
                              list={[o.a,o.b,o.c,o.d]}
                              isVisible={true}
                              onClose={this.closePopover}
                              correctAns={o.ans}
                              desc={o.desc}
                            />
                          </View>
                          <Text style={[Styles.chatTextBy,{position:"absolute",right:5,bottom:0}]}>
                            {this.getTime(rowData.created_at)}
                          </Text>
                        </View>
                    </View>
                  );
              }
          }
          catch(e){
              console.log(e);
              return (
                <View style={{flex:1,flexDirection:"row",width:(3*DeviceWidth/4),marginTop:10,marginLeft:5}}>
                  <Icon name="arrow-left-b" size={25} color="white" />
                  <View style={[{width:(3*DeviceWidth/4)-25,paddingLeft:10,paddingRight:10,paddingTop:10,paddingBottom:20,marginLeft:-1,
                    backgroundColor:"white",borderRadius:5,flexDirection:"column"} ]}>
                    <Text style={Styles.chatTextBy}>{rowData.user}</Text>
                    <Text style={Styles.chatText}>{rowData.msg}</Text>
                    <Text style={[Styles.chatTextBy,{position:"absolute",right:5,bottom:0}]}>
                      {this.getTime(rowData.created_at)}
                    </Text>
                  </View>
              </View>
            );
          }

    }
    
    },

  sendQuestion: function(){
    var q = this.state._question_;
    var answers = {
      1: this.state._opt1_,
      2: this.state._opt2_,
      3: this.state._opt3_,
      4: this.state._opt4_
    }
    var desc = this.state._desc_
    var solutionOrder = [1,2,3,4].sort(function() {
      return .5 - Math.random();
    });
    var msg = {
      q: q,
      desc:desc,
      a:answers[solutionOrder[0]],
      b:answers[solutionOrder[1]],
      c:answers[solutionOrder[2]],
      d:answers[solutionOrder[3]],
      ans:""+solutionOrder.indexOf(1)
    }
    this.sendMessage(JSON.stringify(msg));
    this.refs._question_.setNativeProps({text: ''})
    this.refs._opt1_.setNativeProps({text: ''})
    this.refs._opt2_.setNativeProps({text: ''})
    this.refs._opt3_.setNativeProps({text: ''})
    this.refs._opt4_.setNativeProps({text: ''})
    this.refs._desc_.setNativeProps({text: ''})
    this.setState({
      modalOpened:false
    })
  },

  closeModal: function(){
    this.setState({
      modalOpened: false,
    })
  },

  render: function() {
    if(!this.state.loaded){
      return (
      <View style={{height:DeviceHeight,width:DeviceWidth,backgroundColor:"#CED4E9"}}>
        <View style={{width:DeviceWidth,position:"absolute",
                    top:70,left:0,right:0,bottom:0 }}>
          <Image
              style={{width:DeviceWidth,height:DeviceHeight,opacity:0.1,position:"absolute",top:-70}}
              source={require('image!dino')} />
          <ScrollView style={{backgroundColor:"transparent"}} 
          keyboardShouldPersistTaps={false} 
          automaticallyAdjustContentInsets={false}>
          </ScrollView>

          <View style={{width:DeviceWidth,height:45,
                flexDirection:"row",backgroundColor:"#4D5B7E",borderTopWidth:5,borderColor:"#4D5B7E"}}>

            <TouchableOpacity style={[Styles.centerALL,{height:35,width:35,marginRight:5,
              backgroundColor:"#EA456B",borderRadius:5,marginLeft:5}]} >
              <Icon name="plus" size={17.5} color="white"/>
            </TouchableOpacity>

            <View style={{width:DeviceWidth-90,backgroundColor:"white",height:35,borderRadius:5}}>
            <TextInput
              style={{width:DeviceWidth-120,backgroundColor:"white",height:35,marginLeft:5}}
              multiline={false}
              editable={false}
              autoFocus={false}/>
            </View>

            <TouchableOpacity style={[Styles.centerALL,{height:35,width:35,marginRight:5,
              backgroundColor:"#EA456B",borderRadius:5,marginLeft:5}]}>
              <Icon name="android-send" size={17.5} color="white" />
            </TouchableOpacity>
          </View>
        </View>

        <View style={{backgroundColor:"#4B5980",height:20}}></View>
        
        <View style={[Styles.centerALL,
          {height:50,width:DeviceWidth,backgroundColor:"#4B5980"}]}>
          
          <TouchableOpacity onPress={this.switchit}
            style={[Styles.centerALL,{position:"absolute",left:0,top:0,width:65,height:50}]}>
            <Icon name="arrow-left-b" size={25} color="white" />
          </TouchableOpacity>
          <Text style={Styles.pageTitle}>{this.state.page}</Text>
          <Text style={Styles.pageSubTitle}>
            Entering room ..
          </Text>
        {/*  <TouchableOpacity onPress={this.refresh}
            style={[Styles.centerALL,{position:"absolute",right:0,top:0,width:65,height:50}]}>
            <Icon name="refresh" size={25} color="white" />
          </TouchableOpacity> */}
        </View>

      </View>
      );
    
    }

    StatusBarIOS.setStyle('light-content');
    
    return (
      <View style={{height:DeviceHeight,width:DeviceWidth,backgroundColor:"#CED4E9"}}>
        <Animated.View style={{width:DeviceWidth,position:"absolute",
                    top:70,left:0,right:0,bottom: this.state.keyboardOffset }}>
          <Image
              style={{width:DeviceWidth,height:DeviceHeight,opacity:0.1,position:"absolute",top:-70}}
              source={require('image!dino')} />
          <ScrollView style={{backgroundColor:"transparent",height:DeviceHeight-115-this.state.keyboardOffset}} 
          keyboardShouldPersistTaps={false}
          automaticallyAdjustContentInsets={false}
          ref="scrollview">
            <ListView
              ref= "listview"
              style={{marginBottom:5,backgroundColor:"transparent",
                  height:DeviceHeight-115-this.state.keyboardOffset}}
              dataSource={this.state.dataSource}
              automaticallyAdjustContentInsets={false}
              pageSize={50}
              initialListSize={50}
              renderRow={this.renderListItem} />
          </ScrollView>

          <View style={{width:DeviceWidth,height:45,
                flexDirection:"row",backgroundColor:"#4D5B7E",borderTopWidth:5,borderColor:"#4D5B7E"}}>

            <TouchableOpacity style={[Styles.centerALL,{height:35,width:35,marginRight:5,
              backgroundColor:"#EA456B",borderRadius:5,marginLeft:5}]} onPress={()=>{this.setState({modalOpened:true})}} >
              <Icon name="plus" size={17.5} color="white" />
            </TouchableOpacity>

            <View style={{width:DeviceWidth-90,backgroundColor:"white",height:35,borderRadius:5}}>
            <TextInput
              style={{width:DeviceWidth-120,backgroundColor:"white",height:35,marginLeft:5}}
              multiline={false}
              ref="textinput"
              autoFocus={false}
              onChangeText={(text) => this.setState({text})}
              value={this.state.text}
              onSubmitEditing= {this.sendMessage.bind(this,this.state.text)} />
            </View>

            <TouchableOpacity style={[Styles.centerALL,{height:35,width:35,marginRight:5,
              backgroundColor:"#EA456B",borderRadius:5,marginLeft:5}]} onPress={this.sendMessage.bind(this,this.state.text )}>
              <Icon name="android-send" size={17.5} color="white" />
            </TouchableOpacity>
          </View>
        </Animated.View>

        <View style={{backgroundColor:"#4B5980",height:20}}></View>
        
        <View style={[Styles.centerALL,
          {height:50,width:DeviceWidth,backgroundColor:"#4B5980"}]}>
          
          <TouchableOpacity onPress={this.switchit} 
            style={[Styles.centerALL,{position:"absolute",left:0,top:0,width:65,height:50}]}>
            <Icon name="arrow-left-b" size={25} color="white" />
          </TouchableOpacity>
          
          <Text style={Styles.pageTitle}>{this.state.page}</Text>
          <Text style={Styles.pageSubTitle}>
            {(this.state.count==0||this.state.count==1)
              ?
            this.state.count+" person in room":this.state.count+" people in room"}
          </Text>
          
         {/* <TouchableOpacity onPress={this.refresh}
            style={[Styles.centerALL,{position:"absolute",right:0,top:0,width:65,height:50}]}>
            <Icon name="refresh" size={25} color="white" />
          </TouchableOpacity> */}

        </View>

         <Modal
          animated={true}
          transparent={true}
          visible={this.state.modalOpened}>
          <View style={{backgroundColor:"white",opacity:1,width:DeviceWidth,height:DeviceHeight}}>
            <View style={[
                Styles.centerALL,
                {position:"absolute",top:1,width:DeviceWidth,height:50,backgroundColor:"transparent"}
              ]}>
              <Text style={{color:"grey",fontSize:13,fontWeight:"bold"}}>Add a question</Text>
            </View>

            <TouchableOpacity onPress={this.closeModal}
                style={[Styles.centerALL,{position:"absolute",top:0,right:0,width:68,height:58}]}>
              <Icon name="close" size={25} color="#D85B6F" />
            </TouchableOpacity>

            <View style={[Styles.centerALL,{position:"absolute",top:50,width:DeviceWidth}]}>
                <TextInput
                  style={{width:DeviceWidth-120,backgroundColor:"#EBEBEB",height:35,marginLeft:60,padding:5,borderRadius:5,fontSize:13}}
                  multiline={false}
                  placeholder="Enter question"
                  autoFocus={false}
                  ref="_question_"
                  onChangeText={(_question_) => this.setState({_question_})}
                  value={this.state._question_} />

                <View style={{flexDirection:"row",margin:5}}>
                <TextInput
                  style={{width:DeviceWidth/2-60-2.5,backgroundColor:"#BBCC6F",height:35,marginRight:5,padding:5,borderRadius:5,fontSize:13}}
                  multiline={false}
                  placeholder="Correct answer option"
                  autoFocus={false}
                  ref="_opt1_"
                  onChangeText={(_opt1_) => this.setState({_opt1_})}
                  value={this.state._opt1_} />
                <TextInput
                  style={{width:DeviceWidth/2-60-2.5,backgroundColor:"#D85B6F",height:35,padding:5,borderRadius:5,fontSize:13}}
                  multiline={false}
                  placeholder="Add another option"
                  autoFocus={false}
                  ref="_opt2_"
                  onChangeText={(_opt2_) => this.setState({_opt2_})}
                  value={this.state._opt2_} />
                </View>

                <View style={{flexDirection:"row",marginBottom:5}}>
                <TextInput
                  style={{width:DeviceWidth/2-60-2.5,backgroundColor:"#D85B6F",height:35,marginRight:5,padding:5,borderRadius:5,fontSize:13}}
                  multiline={false}
                  placeholder="Add another option"
                  autoFocus={false}
                  ref="_opt3_"
                  onChangeText={(_opt3_) => this.setState({_opt3_})}
                  value={this.state._opt3_} />
                <TextInput
                  style={{width:DeviceWidth/2-60-2.5,backgroundColor:"#D85B6F",height:35,padding:5,borderRadius:5,fontSize:13}}
                  multiline={false}
                  placeholder="Add another option"
                  autoFocus={false}
                  ref="_opt4_"
                  onChangeText={(_opt4_) => this.setState({_opt4_})}
                  value={this.state._opt4_} />
                </View>

                <TextInput
                  style={{width:DeviceWidth-120,backgroundColor:"#EBEBEB",height:35,marginLeft:60,padding:5,borderRadius:5,fontSize:13,marginBottom:5}}
                  multiline={false}
                  maxLength={80}
                  placeholder="Very short Desciption or Tip to solve"
                  autoFocus={false}
                  ref="_desc_"
                  onChangeText={(_desc_) => this.setState({_desc_})}
                  value={this.state._desc_} />

                <TouchableOpacity onPress={this.sendQuestion}>
                  <View style={[Styles.centerALL,{width:DeviceWidth-120,backgroundColor:"#4D5B7E",
                      height:35,padding:5,borderRadius:5}]}>
                    <Text style={{color:"white",fontSize:13}}>Done !</Text>
                  </View>
                </TouchableOpacity>
            </View>

          </View>
        </Modal>


      </View>
    );
  }
});

module.exports = chatroom;
